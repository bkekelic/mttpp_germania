import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations. AfterMethod ;
import org.testng.annotations. BeforeMethod ;
import org.testng.annotations. Test ;

//WebElement element = driver.findElement(By.xpath(""));

public class MenuTest {

    public WebDriver driver ;
    public String testURL = "https://www.germaniasport.hr/hr#/";

    @BeforeMethod
    public void setupTest (){
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
        //Potrebno je prosiriti ekran kako bi menu tipke bile dostupne
        driver.manage().window().maximize();
    }

    @Test
    public void germaniaMenu() throws InterruptedException {

        WebElement menuKladenje = driver.findElement(By.xpath("//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[1]/nav[@class='navigation']//a[@href='#/betting']/span[.='Klađenje']"));
        menuKladenje.click();
        Thread.sleep(1000);

        WebElement menuUzivo = driver.findElement(By.xpath("//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[1]/nav[@class='navigation']//a[@href='#/live/sport/all']/span[.='Uživo']"));
        menuUzivo.click();
        Thread.sleep(1000);

        WebElement menuLoto = driver.findElement(By.xpath("//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[1]/nav[@class='navigation']//a[@href='#/lotto']/span[.='Loto']"));
        menuLoto.click();
        Thread.sleep(1000);

        WebElement menuLucky = driver.findElement(By.xpath("//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[1]/nav[@class='navigation']//a[@href='#/lucky']/span[.='Lucky']"));
        menuLucky.click();
        Thread.sleep(1000);

        WebElement menuHomePage = driver.findElement(By.xpath("/html//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[1]/nav[@class='navigation']//a[@href='#/']"));
        menuHomePage.click();
        Thread.sleep(1000);

    }

    @AfterMethod
    public void teardownTest (){
        driver.quit();
    }
}