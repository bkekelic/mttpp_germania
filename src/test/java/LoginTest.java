import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations. AfterMethod ;
import org.testng.annotations. BeforeMethod ;
import org.testng.annotations. Test ;

public class LoginTest {

    public WebDriver driver ;
    public String testURL = "https://www.germaniasport.hr/hr#/";
    public String userName = "berky9";
    public String pass = "mttpp";

    @BeforeMethod
    public void setupTest (){
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }

    @Test
    public void germaniaLogin() throws InterruptedException {

        //prijava

        WebElement loginBtn = driver.findElement(By.xpath("//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[2]/article[@class='logged-out-group']/a[@class='login-link']"));
        loginBtn.click();
        Thread. sleep ( 2000 );

        WebElement userNameField = driver.findElement(By.xpath("/html//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[2]/article[@class='logged-out-group']//input[@name='username']"));
        WebElement passwordField = driver.findElement(By.xpath("/html//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[2]/article[@class='logged-out-group']//input[@name='password']"));
        WebElement prijaviSeBtn = driver.findElement(By.xpath("/html//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[2]/article[@class='logged-out-group']//a[@class='login-button']"));

        userNameField.sendKeys(userName);
        Thread. sleep ( 1000 );

        passwordField.sendKeys(pass);
        Thread. sleep ( 1000 );

        prijaviSeBtn.click();
        Thread. sleep ( 2000 );


        //odjava

        WebElement userNameBtn = driver.findElement(By.xpath("/html//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[2]//p[@class='regular']"));
        userNameBtn.click();
        Thread. sleep ( 500 );

        WebElement logOutBtn = driver.findElement(By.xpath("/html//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[2]//p[.='Odjava']"));
        logOutBtn.click();
        Thread.sleep(2000);

    }

    @AfterMethod
    public void teardownTest (){
        driver.quit();
    }
}