import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations. AfterMethod ;
import org.testng.annotations. BeforeMethod ;
import org.testng.annotations. Test ;

import java.util.Random;

//WebElement element = driver.findElement(By.xpath(""));

public class ResultTest {

    public WebDriver driver ;
    public String testURL = "https://www.germaniasport.hr/hr/germania-rezultati#/";

    @BeforeMethod
    public void setupTest (){
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
        driver.manage().window().maximize();
    }

    @Test
    public void germaniaResults() throws InterruptedException {

        //odaberi sport - nogomet
        WebElement rezElement = driver.findElement(By.xpath("/html//ul[@id='acc']/li[1]/div[@class='check-box-wrapper']/label"));
        rezElement.click();
        Thread.sleep(500);

        //pogledaj rezultate od jucer
        rezElement = driver.findElement(By.xpath("//div[@id='page']/section[@class='container finished']//ul[@class='hold3']/li[2]/a[@href='javascript:void(0)']//span[@class='small']"));
        rezElement.click();
        Thread.sleep(500);

        //sortiraj po natjecanju
        rezElement = driver.findElement(By.xpath("//div[@id='page']/section[@class='container finished']//ul[@class='hold1']/li[1]/a[@href='javascript:void(0)']/div"));
        rezElement.click();
        Thread.sleep(500);

        //vrati se na pocetnu stranicu
        rezElement = driver.findElement(By.xpath("/html//div[@id='navigationVue']//header/section[1]/nav[@class='navigation']//a[@href='/']"));
        rezElement.click();
        Thread.sleep(3000);




    }

    @AfterMethod
    public void teardownTest (){
        driver.quit();
    }
}