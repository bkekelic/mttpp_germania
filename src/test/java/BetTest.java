import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations. AfterMethod ;
import org.testng.annotations. BeforeMethod ;
import org.testng.annotations. Test ;

import java.util.Random;

//WebElement element = driver.findElement(By.xpath(""));

public class BetTest {

    public WebDriver driver ;
    public String testURL = "https://www.germaniasport.hr/hr#/betting";

    @BeforeMethod
    public void setupTest (){
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
        driver.manage().window().maximize();
    }

    @Test
    public void germaniaBet() throws InterruptedException {

        //Otvori ponude iz nogometa
        WebElement betElement = driver.findElement(By.xpath("/html//section[@id='left']/div[@class='bar-bar vb vb-invisible']/div[@class='vb-content']/div/div[3]/div[2]/ul/li[1]/div"));
        betElement.click();
        Thread.sleep(500);



        //Odaberi ponudu iz prvog ponudenog natjecanja
        betElement = driver.findElement(By.xpath("/html//section[@id='left']/div[@class='bar-bar vb vb-visible']/div[@class='vb-content']/div/div[3]/div[2]/ul/li[1]/ul/li[1]/span[@class='name']"));
        betElement.click();
        Thread.sleep(1000);

        //Odaberi prvi par
        betElement = driver.findElement(By.xpath("/html//div[@id='vbarCentral']/div[@class='vb-content']/section[1]//article//div[@title='Tim 2 će pobijediti na utakmici']/span[.='1.80']"));
        betElement.click();
        Thread.sleep(1000);



        //Odaberi ponudu iz drugog ponudenog natjecanja
        betElement = driver.findElement(By.xpath("/html//section[@id='left']/div[@class='bar-bar vb vb-visible']/div[@class='vb-content']/div/div[3]/div[2]/ul/li[1]/ul/li[2]/span[@class='name']"));
        betElement.click();
        Thread.sleep(1000);

        //Odaberi drugi par
        betElement = driver.findElement(By.xpath("/html//div[@id='vbarCentral']/div[@class='vb-content']/section[1]/div[@class='sportsoffer']/div[3]/article//div[@title='Tim 1 će pobijediti na utakmici']/span[.='2.50']"));
        betElement.click();
        Thread.sleep(1000);



        //Odaberi ponudu iz treceg ponudenog natjecanja
        betElement = driver.findElement(By.xpath("/html//section[@id='left']/div[@class='bar-bar vb vb-visible']/div[@class='vb-content']/div/div[3]/div[2]/ul/li[1]/ul/li[3]/span[@class='name']"));
        betElement.click();
        Thread.sleep(1000);

        //Odaberi treci par
        betElement = driver.findElement(By.xpath("/html//div[@id='vbarCentral']/div[@class='vb-content']/section[1]/div[@class='sportsoffer']/div[4]/article//div[@title='Tim 1 će pobijediti na utakmici']/span[.='1.30']"));
        betElement.click();
        Thread.sleep(1000);




        //Izbrisi prvi par s listica
        betElement = driver.findElement(By.xpath("//html//article[@id='testprint']/div[@class='matches']/span/div[1]/div[@class='center-part']/div[@class='upper-part']/span[@class='clear-button']"));
        betElement.click();
        Thread.sleep(500);

        //Postavi ulog
        betElement = driver.findElement(By.xpath("/html//input[@id='bettingAmount']"));
        betElement.clear();
        betElement.sendKeys("10");
        Thread.sleep(500);

        //Stisni uplata
        betElement = driver.findElement(By.xpath("/html//button[@id='pay-ticket']"));
        betElement.click();
        Thread.sleep(500);

        //Potvrdi uplatu
        betElement = driver.findElement(By.xpath("//section[@id='right']//div[@class='vb-content']/section/section[@class='bettingticket']/section[@class='ticket-holder']/section[2]/section/section[@class='table-wrapper']//button[@class='button pay']"));
        betElement.click();
        Thread.sleep(1000);

    }

    @AfterMethod
    public void teardownTest (){
        driver.quit();
    }
}