import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations. AfterMethod ;
import org.testng.annotations. BeforeMethod ;
import org.testng.annotations. Test ;

import java.util.Random;

//WebElement element = driver.findElement(By.xpath(""));

public class PlacesTest {

    public WebDriver driver ;
    public String testURL = "https://www.germaniasport.hr/hr#";

    @BeforeMethod
    public void setupTest (){
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
        driver.manage().window().maximize();
    }

    @Test
    public void germaniaPlaces() throws InterruptedException {


        WebElement placeElement = driver.findElement(By.xpath("/html//div[@id='pageWrapper']/section[@class='all-togheter']//div[@class='main-bar vb vb-visible']//section[@class='widget-footer']/article[2]//a[@href='/hr/kontakt/poslovnice']"));
        placeElement.click();
        Thread.sleep(500);
/*
        placeElement = driver.findElement(By.xpath(""));
        placeElement.click();
        Thread.sleep(500);
*/
        //pogledaj kladionice i kladomate u Osijeku
        placeElement = driver.findElement(By.xpath("//div[@id='page']/section[@class='container']/div/div[4]/header[@class='mozzart-places-header']"));
        placeElement.click();
        Thread.sleep(500);


        //pretrazi kladomate u Pozegi
        placeElement = driver.findElement(By.xpath("/html//input[@id='map-search']"));
        placeElement.sendKeys("Požega");
        placeElement.sendKeys(Keys.ENTER);
        Thread.sleep(2000);

        //pretrazi kladomate u Zagrebu
        placeElement = driver.findElement(By.xpath("/html//input[@id='map-search']"));
        placeElement.clear();
        placeElement.sendKeys("Zagreb");
        placeElement.sendKeys(Keys.ENTER);
        Thread.sleep(2000);


    }

    @AfterMethod
    public void teardownTest (){
        driver.quit();
    }
}