import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations. AfterMethod ;
import org.testng.annotations. BeforeMethod ;
import org.testng.annotations. Test ;

//WebElement element = driver.findElement(By.xpath(""));

public class TeletextTest {

    public WebDriver driver ;
    public String testURL = "https://www.germaniasport.hr/hr#/";

    @BeforeMethod
    public void setupTest (){
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
        driver.manage().window().maximize();
}

    @Test
    public void germaniaTeletext() throws InterruptedException {

        WebElement menuTeletext = driver.findElement(By.xpath("//div[@id='pageWrapper']//header[@class='header-navigation navigation-push']/section[1]/nav[@class='navigation']//a[@href='/hr/teletext']/span[.='Teletext']"));
        menuTeletext.click();


        WebElement txtElement;

        //Otvaranje teletext stranica 729, 730, 731, 732, 733, 734, 735, 736 i 737
        for(int i = 2;i<=9;i++){

            txtElement = driver.findElement(By.xpath("//section[@id='teletext-container']/nav/span["+ i +"]"));
            txtElement.click();
            Thread.sleep(500);
        }

        //Otvaranje teletext stranica 737-729
        for(int i = 8;i>=1;i--){

            txtElement = driver.findElement(By.xpath("//section[@id='teletext-container']/nav/span["+ i +"]"));
            txtElement.click();
            Thread.sleep(500);
        }



    }

    @AfterMethod
    public void teardownTest (){
        driver.quit();
    }
}