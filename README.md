Kolegij: Metode i tehnike testiranja programske podr�ke
Student: Bernard Kekelic
Web stranica: https://www.germaniasport.hr/hr#/

Informacije o kori�tenim alatima:
Prije izrade samog projekta bilo je potrebno skinuti chromewebdriver te njegovu putanju spremiti
u sistemsku varijablu. 
Za uspje�no testiranje gore navedene web stranice kori�teno je razvojno okru�enje IntelliJ.
Kreiran je novi Maven projekt, te je za pokretanje testova kori�ten POM.XML okvir za testiranje koji
slu�i kako bi se testovi pokretali jedan nakon drugog.

	

Test1:	"LoginTest"
	Testiranje prijave s ispravnim korisnickim imenom i lozinkom, te odjava iz sustava. Za
	uspje�nu prijavu potrebno je izmjeniti java datoteku te upisati ispravno korisnicko ime
	i lozinku.

Test2:	"MenuTest"
	Testiranje menu izbornika web stranice - prijelaz iz jednog taba u drugi.

Test3:	"TeletextTest"
	Testiranje teletext stranica web stranice, od txt stranice 729 do 737, te i u suprotnom smjeru.

Test4:	"BetTest"
	Testiranje odabira parova, brisanja parova, unosa uloga i odabira potvrde o uplati.

Test5:	"PlacesTest"
	Testiranje prikaza kladomata i kladionica - prema ponudenim mjestima i prema opciji pretra�ivanja

Test6:	"ResultTest"
	Testiranje prikaza rezultata nogometnih utakmica, te povratak na pocetnu stranicu.